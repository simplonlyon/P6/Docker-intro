## Installation

To run the app, you need to have docker engine and docker-compose installed.

### Docker engine

Remove docker older version :

```shell
sudo apt-get remove docker docker-engine docker.io
```

As always, apt update before anything ... :

```shell
sudo apt-get update -y
```

Install packages to allow apt to use a repository over HTTPS :

```shell
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
```

Add GPG key :

```shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Add repository :

```shell
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

update apt list :

```shell
sudo apt-get update -y
```

install docker latest version :

```shell
sudo apt-get install -y docker-ce
```

source : https://docs.docker.com/install/linux/docker-ce/ubuntu/

### Docker compose

remove previous docker-compose version :

```shell
sudo rm -rf /usr/local/bin/docker-compose
```

download docker compose in /usr/local/bin/docker-compose :
```sh
  sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
```
make docker-compose executable :

```shell
sudo chmod +x /usr/local/bin/docker-compose
```

Installing command completion :

```shell
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.21.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

Add current user to docker group  :

```shell
sudo usermod -a -G docker $USER
```

... And reboot :

```shell
reboot
```

source : https://docs.docker.com/compose/install/